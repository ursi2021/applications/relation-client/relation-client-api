let controller = {};
const models = require('../models/model');
const logger = require("../logs");
const axios = require("axios");
module.exports = controller;

controller.getClientAccount = async function(req, res){
    try {
        let client;
        if (!req.params.account){
            client = await models.ClientAccount.findAll({
                include: {
                    model: models.ClientAddress,
                    as: 'clientAccountId'
                }
            })
        }
        else {
            client = await models.ClientAccount.findAll({
                where: {
                    account: req.params.account
                },
                include: {
                    model: models.ClientAddress,
                    as: 'clientAccountId'
                }
            });
        }
        let result = [];
        let len = 1;
        if (client.length)
            len = client.length;
        for (let i = 0; i < len; i++) {
            let clientAddresses = await models.ClientAddress.findAll({
                where: {
                    clientAccountId: client[i].id
                }
            });
            let addressesClient = [];
            for (let j = 0; j < clientAddresses.length; j++) {
                addressesClient = await models.Address.findAll({
                        where: {
                            id: clientAddresses[j].addressId
                        },
                        include: {
                            model: models.ClientAddress,
                            as: 'clientAddressId'
                        }
                    }
                );
            }
            result.push({
                id: client[i].id,
                account: client[i].account,
                "last-name": client[i]["last-name"],
                "first-name": client[i]["first-name"],
                civility: client[i].civility,
                "customer-loyalty": client[i]["customer-loyalty"],
                credit: client[i].credit,
                payment: client[i].payment,
                "address-list": addressesClient.map((address) => ({
                    country: address.country,
                    city: address.city,
                    zipcode: address.zipcode,
                    street: address.street,
                    "street_number": address["street_number"]
                }))
            })
        }
        res.json(result)
    } catch (e) {
        logger.error("Error while getting the client account : " + e.toString());
        res.status(500);
    }
}

controller.create_account = async function (req, res) {
    req.body.map(async (current) => {
        const my_client = await models.ClientAccount.create({
            "last-name":current["last-name"],
            "civility":current.civility,
            "first-name":current["first-name"],
            "credit":0,
            "email":current.email,
            "payment":0,
            "customer-loyalty":0,
            "account":current.email,
            "card-number":current["card-number"],
            "ccv":current.ccv,
            "validity-date":current["validity-date"],
            "provider":current.provider,
        })
        for (const element of current.address) {
            const my_address = await models.Address.create({
                country:element.country,
                city:element.city,
                zipcode:element.zipcode,
                street:element.street,
                "street_number":element['street_number'],
            })
            await models.ClientAddress.create({
                clientAccountId:my_client.id,
                addressId:my_address.id
            })
        }
        res.status(200).json();
    })
};

controller.state_order = async function (req, res) {
    try {
        let orderList = req.data

        let orders = await models.Order.findAll({
            where: {
                clientAccount: req.params.account
            }
        });

        for (let i = 0; i < orders.length; i++) {
            for (let j = 0; j < orderList.length; j++) {
                if (orderList[j].orderId == orders[i])
                {
                    await orders[i].update({
                        "state": orderList[j]["state-order"]
                    })
                    break;
                }
            }
        }
    } catch (e) {
        logger.error("Error while receiving order list via POST : " + e);
        res.status(500).send("Internal Error");
    }
}


controller.storeWebSale = async function (req, res) {
    try {
        let sale = req.body
        console.log("Before order")

        let clientaccount = await models.ClientAccount.findOne({
            where: {
                account: sale.account
            }
        })

        //Update client informations
        await models.Order.create({
            "id-order": sale.id,
            clientAccountId: clientaccount.id
        })
        console.log("After order")


        await clientaccount.update({
            "customer-loyalty": clientaccount["customer-loyalty"] + sale["sum-price"] * 0.005
        })
        console.log("After Update")
        res.status(200).json();

    } catch (e) {
        logger.error("Error while receiving webSale via POST : " + e);
        res.status(500).send("Internal Error");
    }
}

controller.getBankDetail = async function (req, res) {
    try {
        let accountId = req.params.account
        //Get the client account
        let client = await models.ClientAccount.findOne({
            where: {
                account: accountId
            }
        })
        let result = [];
        result = await models.CreditCard.findAll({
          where: {
            ClientAccountId: client.id,
          },
        });
        res.json(result)
    } catch (e) {
        logger.error("Error while GET Bank Detail : " + e);
        res.status(500).send("Internal Error " + e);
    }
}
