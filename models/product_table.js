const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
  host: process.env.URSI_DB_HOST,
  port: Number(process.env.URSI_DB_PORT),
  logging: msg => logger.verbose(msg),
  dialectOptions: {
    timezone: 'Etc/GMT0'
  },
  dialect: 'mariadb'
});

try {
  sequelize.authenticate();
  logger.info('Connection has been established successfully.');
} catch (error) {
  logger.error('Unable to connect to the database:', error);
}

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          firstName:
 *              type: string
 *          lastName:
 *              type: string
 */
const Product = sequelize.define('Product', {
  // Model attributes are defined here
  codeProduit: {
    type: DataTypes.STRING,
    allowNull: false
  },
  familleProduit: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  descriptionProduit: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  quantiteMin: {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  packaging: {
    type: DataTypes.INTEGER
    // allowNull defaults to true
  },
  prix: {
    type: DataTypes.FLOAT
    // allowNull defaults to true
  },

}, {
  timestamps: false
  // Other model options go here
});


Product.sync();
logger.info("The table for the Product model was just (re)created!");
var _app = {};
module.exports = function(app){
  _app = app;
  return Product
};