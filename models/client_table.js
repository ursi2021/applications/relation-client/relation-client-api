const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
  host: process.env.URSI_DB_HOST,
  port: Number(process.env.URSI_DB_PORT),
  logging: msg => logger.verbose(msg),
  dialectOptions: {
    timezone: 'Etc/GMT0'
  },
  dialect: 'mariadb'
});

try {
  sequelize.authenticate();
  logger.info('Connection has been established successfully.');
} catch (error) {
  logger.error('Unable to connect to the database:', error);
}

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          Nom:
 *              type: string
 *          Prenom:
 *              type: string
 *          Credit:
 *              type: float
 *          Paiement:
 *              type: int
 *          Compte:
 *              type: string
 */
const Client = sequelize.define('Client', {
  // Model attributes are defined here
  firstname: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastname: {
    type: DataTypes.STRING,
    allowNull: false
  },
  dateOfBirth:{
    type: DataTypes.STRING,
    allowNull: false
  },
  civility:{
    type: DataTypes.STRING,
    allowNull: false
  },
  credit: {
    type : DataTypes.FLOAT,
    allowNull: false
  },
  civility:{
    type : DataTypes.STRING
  },
}, {
  timestamps: false
  // Other model options go here
});


Client.sync();
logger.info("The table for the Client model was just (re)created!");
var _app = {};
module.exports = function(app){
  _app = app;
  return Client
};