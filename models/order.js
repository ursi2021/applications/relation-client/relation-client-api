const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  Order:
 *      type: object
 *      properties:
 */

function model(sequelize) {
    return sequelize.define('order', {
        // Model attributes are defined here
        "id-order": {
            type: DataTypes.INTEGER,
        },
        "state" : {
            type: DataTypes.STRING,
            validate: {
                isState(value) {
                    if (value !== "In preparation" && value !== "Shipped" && value !== "Delivered" && value !== "Canceled") {
                        throw new Error(" Please enter a good state. States are : Shipped, Delivered & Canceled")
                    }
                }
            },
            allowNull: false,
            defaultValue: "In preparation"
        }
    }, {
        sequelize, modelName: 'order',
        timestamps: false
        // Other model options go here
    });
}