const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  Address:
 *      type: object
 *      properties:
 *          country:
 *              type: string
 *          city:
 *              type: string
 *          zipcode:
 *              type: string
 *          street:
 *              type: string
 *          street-number:
 *              type: string
 *
 */
function model(sequelize) {
    return sequelize.define('address', {
        // Model attributes are defined here
        country: {
            type: DataTypes.STRING,
            allowNull: false
        },
        city: {
            type: DataTypes.STRING,
            allowNull: false
        },
        zipcode: {
            type: DataTypes.STRING,
            allowNull: false
        },
        street: {
            type: DataTypes.STRING,
            allowNull: false
        },
        "street_number": {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        sequelize, modelName: 'address',
        timestamps: false
        // Other model options go here
    });
}