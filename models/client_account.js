const { DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  ClientAccount:
 *      type: object
 *      properties:
 *          account:
 *              type: string
 *          last-name:
 *              type: string
 *          first-name:
 *              type: string
 *          civility:
 *              type: string
 *          customer-loyalty:
 *              type: float
 *          credit:
 *              type: float
 *          payment:
 *              type: int
 *          email:
 *              type: string
 *          password:
 *              type: string
 */
function model(sequelize) {
    return sequelize.define('client-account', {
        // Model attributes are defined here
        account: {
            type: DataTypes.STRING,
            allowNull: false
        },
        "last-name": {
            type: DataTypes.STRING,
            allowNull: false
        },
        "first-name": {
            type: DataTypes.STRING,
            allowNull: false
        },
        "civility": {
            type: DataTypes.STRING,
            allowNull: false
        },
        "customer-loyalty": {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        "credit": {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        "payment": {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        "email": {
            type: DataTypes.STRING,
            allowNull: true
        },
        "password": {
            type: DataTypes.STRING,
            allowNull: true
        },
        "create-at":{
            type: DataTypes.DATE,
            allowNull: true,
        },
        "update-at": {
            type: DataTypes.DATE,
            allowNull: true
        },
        "card-number": {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        "validity-date": {
            type: DataTypes.STRING,
            allowNull: true
        },
        "ccv": {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        "provider" : {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        sequelize, modelName: 'client',
        timestamps: false
        // Other model options go here
    });
}