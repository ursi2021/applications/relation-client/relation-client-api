const {Sequelize} = require('sequelize');
/*const stockPositionsJson = require("./samples/stockPositions.json")
let stockPositionsList = stockPositionsJson.stockPositions*/
const logger = require('../logs')

let models = {};
module.exports = models;

initialize()

async function initialize() {
    const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER, process.env.URSI_DB_PASSWORD, {
        host: process.env.URSI_DB_HOST,
        port: Number(process.env.URSI_DB_PORT),
        logging: msg => logger.verbose(msg),
        dialectOptions: {
            timezone: 'Etc/GMT0'
        },
        dialect: 'mariadb'
    });

    // Init models and add them to the exported db object.
    models.ClientAccount = require('./client_account')(sequelize);
    models.Address = require('./address')(sequelize);
    models.Order = require('./order')(sequelize);
    models.Bill = require('./bill')(sequelize);
    models.ClientAddress = require('./client_address')(sequelize);

    // Define associations.
    //models.ClientAccount.belongsToMany(models.Address, { through: 'client-address' });
    //models.Address.belongsToMany(models.ClientAccount, { through: 'client-address' });
    models.ClientAccount.hasMany(models.ClientAddress, {as: "clientAccountId"});
    models.Address.hasMany(models.ClientAddress, {as: "clientAddressId"});
    models.ClientAccount.hasMany(models.Order, {as: "order"});
    models.ClientAccount.hasMany(models.Bill, {as: "bill"});

    // Sync all models with database.
    await sequelize.sync();
    logger.info("The table for models was just (re)created!");

    // Set database with Clients Data
    await setClientData();
}

async function setClientData() {
    logger.debug("Fill Database with Client informations")
    await setClientAccountData();
    await setAddress();
    //await setBill();
    await setOrder();
    await setClientAddress();
}

async function setClientAccountData() {
    var insert = require('./json/client_account.json');
    logger.debug("Fill ClientAccount Database");
    models.ClientAccount.findAll().then(prod => {
        if (prod.length == 0) {
            const c = models.ClientAccount.bulkCreate(insert).then (res => {
            }).catch(function (e){
                console.log(e);
            });
        }
    }).catch(function (e) {
        console.log(e);
    })
}

async function setAddress() {
    var insert = require('./json/address.json');
    logger.debug("Fill Address Database");
    models.Address.findAll().then(prod => {
        if (prod.length == 0) {
            const c = models.Address.bulkCreate(insert).then (res => {
            }).catch(function (e){
                console.log(e);
            });
        }
    }).catch(function (e) {
        console.log(e);
    })
}

async function setBill() {
    var insert = require('./json/bill.json');
    logger.debug("Fill Bill Database");
    models.Bill.findAll().then(prod => {
        if (prod.length == 0) {
            const c = models.Bill.bulkCreate(insert).then (res => {
            }).catch(function (e){
                console.log(e);
            });
        }
    }).catch(function (e) {
        console.log(e);
    })
}

async function setOrder() {
    var insert = require('./json/order.json');
    logger.debug("Fill Order Database");
    models.Order.findAll().then(prod => {
        if (prod.length == 0) {
            const c = models.Order.bulkCreate(insert).then (res => {
            }).catch(function (e){
                console.log(e);
            });
        }
    }).catch(function (e) {
        console.log(e);
    })
}


async function setClientAddress() {
    var insert = require('./json/client_address.json');
    logger.debug("Fill ClientAdress Database");
    models.ClientAddress.findAll().then(prod => {
        if (prod.length == 0) {
            const c = models.ClientAddress.bulkCreate(insert).then (res => {
            }).catch(function (e){
                console.log(e);
            });
        }
    }).catch(function (e) {
        console.log(e);
    })
}
