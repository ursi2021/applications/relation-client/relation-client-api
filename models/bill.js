const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  Bill:
 *      type: object
 *      properties:
 */

function model(sequelize) {
    return sequelize.define('bill', {
        // Model attributes are defined here
        "id-bill": {
            type: DataTypes.INTEGER,
        }
    }, {
        sequelize, modelName: 'bill',
        timestamps: false
        // Other model options go here
    });
}