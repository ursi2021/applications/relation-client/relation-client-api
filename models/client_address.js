const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  Address:
 *      type: object
 *      properties:
 *          clientAccountId:
 *              type: int
 *          addressId:
 *              type: int
 *
 */
function model(sequelize) {
    return sequelize.define('client-address', {
        // Model attributes are defined here
        clientAccountId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        addressId: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        sequelize, modelName: 'client-addresses',
        timestamps: false
        // Other model options go here
    });
}