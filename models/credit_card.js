const { Sequelize, DataTypes } = require('sequelize');

module.exports = model;

/**
 * @swagger
 * definitions:
 *  CreditCard:
 *      type: object
 *      properties:
 *          id-client:
 *              type: int
 *          card-number:
 *              type: int
 *          validity-date:
 *              type: date
 *          ccv:
 *              type: int
 */
function model(sequelize) {
    return sequelize.define('credit-card', {
        // Model attributes are defined here
        "card-number": {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        "validity-date": {
            type: DataTypes.STRING,
            allowNull: false
        },
        "ccv": {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        "provider" : {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        sequelize, modelName: 'credit-card',
        timestamps: false
        // Other model options go here
    });
}