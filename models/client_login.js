const logger = require('../logs')

const { Sequelize, DataTypes } = require('sequelize');

var insert = require('./clientLogin.json')

// login to mariadb database
// ex : new  new Sequelize(<dbname>, <user>, <password>, ...);
const sequelize = new Sequelize(process.env.URSI_DB_NAME, process.env.URSI_DB_USER,process.env.URSI_DB_PASSWORD, {
    host: process.env.URSI_DB_HOST,
    port: Number(process.env.URSI_DB_PORT),
    logging: msg => logger.verbose(msg),
    dialectOptions: {
        timezone: 'Etc/GMT0'
    },
    dialect: 'mariadb'
});

/**
 * @swagger
 * definitions:
 *  User:
 *      type: object
 *      properties:
 *          Account:
 *              type: string
 *          Email:
 *              type: string
 *          Password:
 *              type: string
 */
const ClientLogin = sequelize.define('client-login', {
    // Model attributes are defined here
    Account: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    Password: {
        type : DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
    // Other model options go here
});

ClientLogin.sync();
logger.info("The table for the Client's Login model was just (re)created!");
ClientLogin.findAll().then(prod =>{
    if (prod.length == 0){
        const c = ClientLogin.create(insert).then ( res => {

        }).catch(function (e) {
            console.log(e);
        });
    }
}).catch(function (e) {
    console.log(e);
});

var _app = {};

module.exports = function (app) {
    _app = app;
    return ClientLogin;
};
