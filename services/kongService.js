const axios = require('axios');
const logger = require('../logs');

var registered = false;

async function register_kong() {
    const registerApp = await axios
        .post('http://kong:8081/services/', {
            name: process.env.APP_NAME,
            url: 'http://' + process.env.APP_NAME
        })
        .then(res => {
            logger.info(`Succesfully register service to kong; statusCode: ${res.statusCode}`)
            return true;
        })
        .catch(error => {
            logger.error('Kong Service register : Axios Error.')
            return false;
        })
    if (!registerApp) return false;
    const registerRoute = await axios
        .post('http://kong:8081/services/' + process.env.APP_NAME + '/routes', {
            paths: ["/" + process.env.APP_NAME],
            name: process.env.APP_NAME
        })
        .then(res => {
            logger.info(`Succesfully register route to kong; tatusCode: ${res.statusCode}`)
            return true;
        })
        .catch(error => {
            logger.error('Kong Route register : Axios Error.')
            return false;
        })
    return registerApp && registerRoute;
}

async function register_process() {
    var test = 0
    while (registered === false && test < 5) {
        test++;
        registered = await register_kong();
    }
}

module.exports = register_process;