const ClientAccount = require('../models/client_account');
class ClientService {
    constructor() {
        this.client = ClientAccount();
    }

    create(paymentId, status, application) {
        if (paymentId == null || status == null || application == null) {
            throw new Error("Bad request body");
        }
        return this.payments.create({
            paymentId,
            status,
            application,
        });
    }

}

export default new ClientService();
