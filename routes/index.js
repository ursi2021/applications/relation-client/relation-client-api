var express = require('express');
var router = express.Router();
var app = express();
var logger = require('../logs');
const axios = require("axios");
const models = require("../models/model");
var User = require('../models/user_template')(app);
var template = require('../controller/template_controller')(app);

const client_account_controller = require('../controller/client_account_controller');

var Client = require('../models/client_table')(app);
/*var ClientAccountList = require('../models/client_account_list')(app);
var ClientLogin = require('../models/client_login')(app);
var ClientBankDetails = require('../models/json/credit_card.json')(app);
var CustomerOrderList = require('../models/customer_order_list')(app);*/

var Product = require('../models/product_table')(app);

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/

router.get('/', template.index)

router.get('/relation-client/page1', template.page1)

/**@swagger
 * /users:
 *   get:
 *     tags:
 *       - Products
 *     summary: get users page
 *     responses:
 *       200:
 *         description: Return users html
 */
router.get('/users', function(req, res, next) {
  //
  User.findAll().then(function(users) {
    logger.info(users);
    res.render('users', {title: 'Users', users: users});
  });
});

/**@swagger
 * /api/user:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of users
 *         schema:
 *           type: array
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/api/user', function(req, res, next) {
  try {
    User.findAll().then(user => res.status(200).json(user));
  } catch (e) {
    res.status(404).json({'error': 'FindAll'});
  }
});

/**@swagger
 * /api/user:
 *   post:
 *     summary: get all users
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Return created user
 *         schema:
 *           $ref: '#/definitions/User'
 *       404:
 *         description: Failed to create data
 */
router.post('/api/user', function(req, res, next) {
  try {
    logger.info(req.body);
    User.create(req.body).then(function (user) {
      res.status(200).json(user);
    });
  } catch (e) {
    res.status(404);
  }
});

const clientsAccount = require('../models/json/client_account.json');
var ClientAccount = require('../models/client_account');


/**@swagger
 *     summary: Send Hello World to other applications
 */

router.get('/relation-client/hello', function(req, res, next) {
  try {
    res.status(200).json({'relation-client': 'Hello World !'})
  } catch (e) {
    res.status(404).json({'error': 'FindAll'});
  }
});


/**@swagger
 * /relation-client/hello/all:
 *   get:
 *     summary: Show Hello World of all applications
 *     tags:
 *       - Hello
 *     responses:
 *       200:
 *         description: get hello world
 */
router.get('/relation-client/' +
    '/all', async  function (req, res, next) {
  var result = [];
  var applications = [
    {appli:'Back office magasin', err: false, url: process.env.URL_BACK_OFFICE_MAGASIN},
    {appli:'Caisse', err: false, url: process.env.URL_CAISSE},
    {appli:'Décisionnel', err: false, url: process.env.URL_DECISIONNEL},
    {appli:'E-commerce', err: false, url: process.env.URL_E_COMMERCE},
    {appli:'Gestion commerciale', err: false, url: process.env.URL_GESTION_COMMERCIALE},
    {appli:'Gestion entrepôts', err: false, url: process.env.URL_ENTREPROTS},
    {appli:'Gestion promotion', err: false, url: process.env.URL_GESTION_PROMOTION},
    {appli:'Monétique paiement', err: false, url: process.env.URL_MONETIQUE_PAIEMENT},
    {appli:'Référentiel produit', err: false, url: process.env.URL_REFERENTIEL_PRODUIT},
    {appli:'Relation client', err: false, url: process.env.URL_RELATION_CLIENT}
  ];
  for (var i = 0; i < applications.length; i++) {
    await axios.get(applications[i].url + '/hello' ).then(function (response) {
      result.push({appli: applications[i].appli, hello: JSON.stringify(response.data), err: applications[i].err});
    }).catch(() => {
      applications[i].err = true;
      result.push({appli: applications[i].appli, hello: 'Error to fetch data', err: applications[i].err});
    });
  }
  res.render('hello', {hello: result});
})

module.exports = router;

/**@swagger
 * /clients:
 *   get:
 *     tags:
 *       - Products
 *     summary: Add Clients page
 *     responses:
 *       200:
 *         description: Return clients html
 */
router.get('/clients', function(req, res, next) {

  var col_name = Object.keys(Client.rawAttributes)

  // Del id
  const index = col_name.indexOf('id');
  if (index > -1) {
    col_name.splice(index, 1);
  }

  //
  Client.findAll().then(function(clients) {
    logger.info(clients);
    res.render('clients', {title: 'Clients', clients: clients, colum_name: col_name});
  });
});

/**@swagger
 * /client-login:
 *   get:
 *     tags:
 *       - ClientLogin
 *     summary: Get Client's Login
 *     responses:
 *       200:
 */
router.get('/client-login', function(req, res, next) {

  var col_name = Object.keys(ClientLogin.rawAttributes)

  // Del id
  const index = col_name.indexOf('id');
  if (index > -1) {
    col_name.splice(index, 1);
  }

  //
  ClientLogin.findAll().then(function(clientlogin) {
    logger.info(clientlogin);
  });
});

/**@swagger
 * /client-bank-details:
 *   get:
 *     tags:
 *       - ClientBankDetails
 *     summary: Get Client's Bank Details
 *     responses:
 *       200:
 */
router.get('/client-bank-details', function(req, res, next) {

  var col_name = Object.keys(ClientBankDetails.rawAttributes)

  // Del id
  const index = col_name.indexOf('id');
  if (index > -1) {
    col_name.splice(index, 1);
  }

  //
  ClientBankDetails.findAll().then(function(clientbankdetails) {
    logger.info(clientbankdetails);
  });
});



/**@swagger
 * /customer-order-list:
 *   get:
 *     tags:
 *       - Customer-order-list
 *     summary: Get Customer's order list
 *     responses:
 *       200:
 */
router.get('/customer-order-list', function(req, res, next) {

  var col_name = Object.keys(CustomerOrderList.rawAttributes)

  // Del id
  const index = col_name.indexOf('id');
  if (index > -1) {
    col_name.splice(index, 1);
  }

  CustomerOrderList.findAll().then(function(customerorderlist) {
    logger.info(customerorderlist);
  });
});


/**@swagger
 * /products:
 *   get:
 *     tags:
 *       - Products
 *     summary: Add Products page
 *     responses:
 *       200:
 *         description: Return products html
 */
router.get('/products', function(req, res, next) {

  var col_name = Object.keys(Product.rawAttributes)

  // Del id
  const index = col_name.indexOf('id');
  if (index > -1) {
    col_name.splice(index, 1);
  }

  Product.findAll().then(function(products) {
    logger.info(products);
    res.render('products', {title: 'Produits', products: products, colum_name: col_name});
  });
});

/**@swagger
 * /relation-client/clients:
 *   get:
 *     tags:
 *       - Products
 *     summary: get all client
 *     consumes:
 *       - application/json
 *     responses:
 *       200:
 *         description: Return an array of client
 *         schema:
 *           type: array
 *           $ref: '#/definitions/Client'
 *       404:
 *         description: Failed to retrieve data
 */
router.get('/relation-client/clients', function(req, res, next) {
  try {
    Client.findAll().then(client => res.status(200).json(client));
  } catch (e) {
    res.status(404).json({'error': 'FindAll'});
  }
});

/**@swagger
 * /relation-client/produits:
 *   get:
 *     tags:
 *       - Products
 *     summary: get products page
 *     responses:
 *       200:
 *         description: Return products html
 */
router.get('/relation-client/produits', async function(req, res, next) {
  var result = '';
  await axios.get(process.env.URL_REFERENTIEL_PRODUIT +'/produits').then(function (products) {
    result = products.data;
  }).catch((err) => {
    result = err.data == undefined ? {"myerror": "My error : return data type /produits is undefined: maybe table is empty"} : err.data;
  });
  res.render('products', {title: 'Produits', products: result});
});

router.post('/api/client', function(req, res, next) {
  logger.info(req.body);
  Client.create(req.body).then(function (client) {

    res.status(200).json(client);
  });

});

router.post('/api/product', function(req, res, next) {
  logger.info(req.body);
  Product.create(req.body).then(function (product) {

    res.status(200).json(product);
  });
});

router.get('/relation-client/client-account-list', client_account_controller.getClientAccount);
router.get('/relation-client/client-account/:account', client_account_controller.getClientAccount);
router.post('/relation-client/new-account', client_account_controller.create_account);
router.post("/relation-client/delivery-client/:account", client_account_controller.state_order);
router.post("/relation-client/data/web-sales", client_account_controller.storeWebSale);
router.post("/relation-client/data/store-sales", client_account_controller.storeWebSale);
router.get("/relation-client/bank-detail/:account", client_account_controller.getBankDetail);